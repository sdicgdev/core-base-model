<?php
namespace Core;

/**
 * represents a "data property"
 * provides data type support
 *
 * @category  Model
 * @version   0.0.1
 * @since     2015-5-22
 * @author  Jim Harney <jim@schooldatebooks.com>
 * @author Deac Karns <deac@sdicg.com>
 * @author Wesley Dekkers <wesly@sdicg.com>
 * @author Matthew Ess <matthew@schooldatebooks.com>
*/
class DataProperty {
  public $name;
  public $type;
  public $required;

  function __construct($name, $type=NULL, $required=FALSE) {
    $this->name = $name;
    if (empty($type)) {
      $type = "STRING";
    }
    $this->type = $type;
    $this->required = $required;
  }
  public function set_type($type) {
    $this->type = strtoupper($type);
  }
  public function get_type() {
    return $this->type;
  }
}

