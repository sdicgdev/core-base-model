<?php
namespace Core;

/**
 * Base model object to be manipulated
 *
 * @category  Model
 * @version   0.0.1
 * @since     2015-10-16
 * @author    Deac Karns <deac@sdicg.com>
 * @author    Wesley Dekkers <wesley@sdicg.com>
*/
class Base {

  public $data_properties;

  function __construct($props=null) {
    $this->data_properties = array();

    if ( isset($props) && is_object($props) && (count(get_object_vars($props)) > 0) ) {
      // set the model object value to that of the incoming post body
      foreach ($props as $key => $value) {
        $this->$key = $value;
      }
    }    

    return $this;
  }
  

  /**
  * defines a data property and adds it to the collection
  *
  * @example
  * <code>
  * $this->define_data_property("id", "STRING", TRUE);
  * $this->define_data_property("title", "STRING", FALSE);
  * $this->define_data_property("description", "STRING", FALSE);
  * </code>
  *
  * @return  VOID
  *
  * @since   2016-5-21
  * @author  Jim Harney <jim@schooldatebooks.com>
  **/
  public function define_data_property($name, $type, $required) {
    $property = $this->get_data_property($name);
    if (empty($property)) {
      $property = new DataProperty($name, $type, $required);
      $this->data_properties[] = $property;
    } else {
      $property->set_type($type);
      if (isset($required)) {
        $propery->required = $required;
      }
    }
  }  

  /**
  * gets a data property by name
  *
  * @example
  * <code>
  * $this->get_data_property("id");
  * </code>
  *
  * @return  DataProperty object or NULL
  *
  * @since   2016-5-21
  * @author  Jim Harney <jim@schooldatebooks.com>
  **/
  public function get_data_property($name) {
    $found_property = NULL;
    foreach($this->data_properties as $property) {
      if ($property->name == $name) {
        $found_property = $property;
        break;
      }
    }
    return $found_property;
  }

  /**
  * gets a data property array
  *
  * @example
  * <code>
  * $data_properties = $this->get_data_properties(TRUE);
  * </code>
  *
  * @return  array of DataProperty objects
  *
  * @since   2016-5-21
  * @author  Jim Harney <jim@schooldatebooks.com>
  **/
  public function get_data_properties($require_isset=FALSE) {
    if ($require_isset) {
      $properties = array();
      foreach ($this->data_properties as $property) {
        $key = $property->name;
        if (isset($this->$key)) {
          $properties[] = $property;
        }
      }
    } else {
      $properties = $this->data_properties;
    }
    return $properties;
  }

  /**
  * gets a data property array for required properties
  *
  * @example
  * <code>
  * $data_properties = $this->get_required_data_properties();
  * </code>
  *
  * @return  array of DataProperty
  *
  * @since   2016-5-21
  * @author  Jim Harney <jim@schooldatebooks.com>
  **/
  public function get_required_data_properties() {
    $required_properties = array();
    foreach($this->data_properties as $property) {
      if ($property->required) {
        $required_properties[] = $property;
      }
    }
    return $required_properties;
  }

  /**
  * checks to see if a given input is a DataProperty object
  *
  * @example
  * <code>
  * $check = self:: is_data_property($something);
  * </code>
  *
  * @return  boolean
  *
  * @since   2016-5-21
  * @author  Jim Harney <jim@schooldatebooks.com>
  **/
  public static function is_data_property($check_me) {
    if ($check_me instanceof DataProperty) {
      return true;
    }
    return false;
  }

  /**
  * checks to see if a given input is an array of DataProperty object
  *
  * @example
  * <code>
  * $check = self:: is_data_properties_array($something);
  * </code>
  *
  * @return  boolean
  *
  * @since   2016-5-21
  * @author  Jim Harney <jim@schooldatebooks.com>
  **/
  public static function is_data_properties_array($check_me) {
    if (is_array($check_me)) {
      if (count($check_me) > 0) {
        return self:: is_data_property($check_me[0]);
      }
    }
    return false;
  }

  /**
  * Reload an object based on only a specified reload key
  *
  * @return **Object**
  *
  * @since   2016-02-02
  * @author  Deac Karns <deac@sdicg.com>
  * @author  Wesley Dekkers <wesley@sdicg.com>
  **/
  public function reload($reload_key = 'id', $exception = true) {
    if (!isset($this->$reload_key) || empty(trim($this->$reload_key))) {
      $error = (object) array("code" => 400, "message" => "The field $reload_key was not set in the object you are attempting to load.");
      \Rhonda\Error::add_summary_item($error);
      if ($exception) {
        throw new \Exception("Base Model: reload failed");
      }
    }

    // remove all keys except the reload key
    $clone = clone $this;
    foreach ($clone as $key => $value) {
      // preserve data properties array
      if ($key != $reload_key && !self:: is_data_properties_array($value)) {
        unset($clone->$key);
      }
    }

    $result = $clone->query();
    if ($exception && count($result) <= 0) {
      $error = (object) array("code" => 404, "message" => "The record you are trying to work with could not be found. [".strtoupper($reload_key)."]");
      \Rhonda\Error::add_summary_item($error);
      if ($exception) {
        throw new \Exception("Base Model: reload failed", 404);
      }
    }
    return array_shift($result);
  }

  /**
  * Reload an active object based on only its id
  *
  * @return **Object**
  *
  * @since   2016-03-14
  * @author  Wesley Dekkers <wesley@sdicg.com>
  **/
  public function reload_active($reload_key = 'id', $exception = TRUE){
    if(!isset($this->$reload_key) || empty(trim($this->$reload_key))){
      $error = (object) array("code" => 400, "message" => "The field $reload_key was not set in the object you are attempting to load.");
      \Rhonda\Error::add_summary_item($error);
      if ($exception) {
        throw new \Exception("Base Model: reload_active failed");
      }
    }
    // remove all keys except the id
    $reload = clone $this;
    foreach ($reload as $key => $value) {
      if($key != $reload_key && !self:: is_data_properties_array($value)){
        unset($reload->$key);
      }
    }
    $reload->active = true;

    $result = $reload->query();
    if($exception && count($result) <= 0){
      $error = (object) array("code" => 404, "message" => "The active record you are trying to work with could not be found. [".strtoupper($reload_key)."]");
      \Rhonda\Error::add_summary_item($error);
      if ($exception) {
        throw new \Exception("Base Model: reload_active failed", 404);
      }
    }
    return array_shift($result);
  }

  /**
  * Search for given parameters and unset empty parameters
  *
  * @return **Array** of result objects
  *
  * @since   2016-02-02
  * @author  Deac Karns <deac@sdicg.com>
  * @author  Wesley Dekkers <wesley@sdicg.com>
  **/
  public function search() {
    // remove all keys where value is empty
    $clone = clone $this;

    //exclude empty data properties from the search filters
    //this means we cannot search for empty fields, ex: lastname = ''
    //this is to protect people from themselves
    foreach ($clone as $key => $value) {
      if (self:: is_data_properties_array($value)) {
        continue;
      }

      if ( 
        (!is_bool($value))
        && (!is_array($value)) 
        && (empty(trim($value)) && $value != "0") 
      ) {
        unset($clone->$key);
      }
    }
    return $clone->query();
  }

  /**
   * Check to see if an object exists based on id
   *
   * @return Boolean
   *
   * @since   2016-01-28
   * @author  Deac Karns <deac@sdicg.com>
   * @author  Wesley Dekkers <wesley@sdicg.com>
   **/
  public function exists($method = 'id', $exception = TRUE, $summary_error = TRUE, $params = FALSE)
  {

    switch ($method) {
      case 'id':

        if(!isset($this->id) || empty($this->id)){
          $error = (object) array("code" => 400, "message" => "The field [ID] was not set.");
          \Rhonda\Error::add_summary_item($error);
          if ($exception) {
            throw new \Exception("Base Model Exists Check: No ID set");
          }
          $exists = FALSE;
        }
        else{

          $id_exists = clone($this);

          foreach ($id_exists as $key => $value) {
          if ($key != 'id' && !self:: is_data_properties_array($value)) {
            unset($id_exists->$key);
          }
        }
        // Check if the object exists by object ID
        $exists = $id_exists->search();
        if (empty($exists)) {
          $error = (object) array("code" => 404, "message" => "The active record you are trying to work with could not be found. [ID]");
          \Rhonda\Error::add_summary_item($error);
          if ($exception) {
            throw new \Exception("Base Model: exists check failed [ID]", 404);
          }
        }
      }
      break;

      case 'id_active':
        if(!isset($this->id) || empty($this->id)){
          // Only register error if we really want to
          if($summary_error){
            $error = (object) array("code" => 400, "message" => "The field [ID] was not set.");
            \Rhonda\Error::add_summary_item($error);
          }
          if ($exception) {
            throw new \Exception("Base Model Exists Check: No ID set");
          }
          $exists = FALSE;
        }
        else{
          $id_active = clone($this);

          foreach($id_active as $key => $value){
            if($key != 'id' && !self:: is_data_properties_array($value)){
              unset($id_active->$key);
            }
          }
          $id_active->active = true; // We add this ot see if there is a active record available
          $exists = $id_active->search();
          if (empty($exists)) {
            if($summary_error){
              $error = (object) array("code" => 404, "message" => "The object you are attempting to modify does not exist or is not active. [ID]");
              \Rhonda\Error::add_summary_item($error);
            }
            if ($exception) {
              throw new \Exception("Base Model: exists check failed [ID]", 404);
            }
            $exists = FALSE;
          }
        }
        break;

      case 'body':
        $exists = $this->search();
        if (empty($exists)) {
          $error = (object) array("code" => 404, "message" => "The object you are attempting to modify does not exist. [BODY]");
          \Rhonda\Error::add_summary_item($error);
          if ($exception) {
            throw new \Exception("Base Model: exists check failed [BODY]", 404);
          }
          $exists = FALSE;
        }
        break;

      case 'body_active':
        $body_active = clone($this);
        $body_active->active = true;
        $exists = $body_active->search();
        if (empty($exists)) {
          $error = (object) array("code" => 404, "message" => "The object you are attempting to modify does not exist or is not active. [BODY]");
          \Rhonda\Error::add_summary_item($error);
          if ($exception) {
            throw new \Exception("Base Model: exists check failed [BODY]", 404);
          }
          $exists = FALSE;
        }
        break;

      case 'duplicate':
        $clone = clone($this);

        # unset internal properties since these should always be unique
        $unset_props = ["id", "created_at", "updated_at"];
        foreach ($unset_props as $prop) {
          if (isset($clone->$prop)) {
            unset($clone->$prop);
          }
        }

        $exists = $clone->search();
        if (!empty($exists)) {
          $error = (object) array("code" => 400, "message" => "Can not create duplicate record.");
          \Rhonda\Error::add_summary_item($error);
          if ($exception) {
            throw new \Exception("Base Model: exists check failed", 400);
          }
        }else{
          $exists = FALSE;
        }
        break;

      case 'params':
        if(!empty($params) && !is_array($params)){
          throw new \Exception("Base Model: exists no valid parameters given", 400);
        }

        $this_clone = clone($this);
        $param_check = TRUE;
        foreach($this_clone as $key => $value){
          if(!in_array($key, $params) && $key != 'data_properties'){
            if(empty($params)){
              $param_check = FALSE;
              if ($exception) {
                throw new \Exception("Base Model: exists check failed [MISSING REQUIRED PARAMETERS]", 400);
              }
              if($summary_error){
                $error = (object) array("code" => 400, "message" => "Base Model: exists check failed [MISSING REQUIRED PARAMETERS]");
                \Rhonda\Error::add_summary_item($error);
              }
            }
            unset($this_clone->$key);
          }
        }
        if($param_check){
          $exists = $this_clone->query();
          if (empty($exists)) {
            if ($exception) {
              throw new \Exception("Base Model: exists check failed [PARAMETERS]", 404);
            }
            if($summary_error){
              $error = (object) array("code" => 404, "message" => "The object you are attempting to modify does not exist [PARAMETERS]");
              \Rhonda\Error::add_summary_item($error);
            }
            $exists = FALSE;
          }
        }
        else{
          // If param check failed
          $exists = FALSE;
        }
        break;

      default:
        throw new \Exception("The exist check [$method] is not implemented.");
        break;
    }
    return $exists;
  }



  /**
  * Checks if required properties exist
  *
  * @return  Throws exception when missing a property
  *
  * @since   2016-02-04
  * @author  Wesley Dekkers <wesley@sdicg.com>
  * @author  Jim Harney <jim@schooldatebooks.com>
  **/
  public function check_required_properties($exception = TRUE){
    foreach ($this->get_required_data_properties() as $property) {
      $key = $property->name;
      $value = $this->$key;

      if (
        (!isset($value)) 
        || (
          (!is_bool($value)) 
          && (empty(trim($value))) 
          && ($value !== '0')
        ) 
      ) {
        $error = (object) array("code" => 400, "message" => "Missing required property: $key.");
        \Rhonda\Error::add_summary_item($error);
        if ($exception) {
          throw new \Exception("Base Model: check_required_properties failed");
        }
        return false;
      }
    }
    return true;
  }

  /**
   * Checks if there is an empty array property
   *
   * @return  Boolean
   *
   * @since   2016-02-04
   * @author  Jim Harney <jim@schooldatebooks.com>
   **/
  public function has_empty_array_property() {
    $result = FALSE;
    foreach ($this as $key => $value) {
      if ($this->is_data_properties_array($value)) {
        continue;
      }
      if ( (is_array($value)) && (count($value) == 0) ) {
        $result = TRUE;
        break;
      }
    }
    return $result;
  }

  /**
  * Validate all set ISO-8601 bound properties
  *
  * @example
  * <code>
  * $this->validate_datetime(STRING);
  * </code>
  *
  * @since   2016-08-25
  * @author  Deac Karns <deac@sdicg.com>
  * @author  Matthew Ess <matthew@schooldatebooks.com>
  **/
  public function validate_datetime() {
    foreach ($this->data_properties as $property) {
      $p = $property->name;
      if($property->type == "ISO8601" && isset($this->$p) ){
        self:: validate_datetime_helper($this->$p, $property->name);
      }
    }
  }

  /**
  * Validate that the incoming date is formatted ISO-8601 format
  *
  * @example
  * <code>
  * $this->validate_datetime_helper(STRING);
  * </code>
  *
  * @since   2016-08-25
  * @author  Deac Karns <deac@sdicg.com>
  * @author  Matthew Ess <matthew@schooldatebooks.com>
  **/
  private function validate_datetime_helper($date, $property=NULL) {
    try {
      $datetime = new \DateTime($date);
      $datetime->format('c');
    } catch (\Exception $e) {
        throw new \Exception("Invalid datetime given. [$property] Should conform to ISO-8601 format yyyy-mm-ddTHH:mm:ss All dates stored in UTC");
    }
  }

}
